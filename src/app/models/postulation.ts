export class Postulation {
  testDate: string;
  testPassed: Boolean;
  admissibility: number;
  note: number;
  decision: string;
  observation: string;
  offreId: number;
  userId: number;
}
