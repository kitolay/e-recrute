export class QCM {
    id: number;
    question: string;
    reponse: number;
    duree: number;
    point: number;
}
