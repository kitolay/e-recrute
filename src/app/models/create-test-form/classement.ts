export class Classement {
    id: number;
    question: string;
    reponse: number;
    duree: number;
    point: number;
}
