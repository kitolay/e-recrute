export class Audio {
    id: number;
    question: string;
    reponse: number;
    duree: number;
    point: number;
}
