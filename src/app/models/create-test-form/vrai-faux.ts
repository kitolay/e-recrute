export class VraiFaux {
    id: number;
    consigne: string;
    question: string;
    reponse: boolean;
    duree: number;
    point: number;
}
