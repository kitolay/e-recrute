export class Choix {
    id: number;
    name: string;
    isAnswer: boolean;
    selected: boolean;
}
